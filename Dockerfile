FROM mongo:4.2.21

RUN apt-get update && apt-get install -y curl

WORKDIR /tmp

RUN curl -O https://repo.percona.com/apt/percona-release_latest.generic_all.deb && \
    apt-get install -y gnupg2 lsb-release ./percona-release_latest.generic_all.deb && \
    rm -rf ./percona-release_latest.generic_all.deb

RUN percona-release enable pbm release && \
    apt-get update && \
    apt-get install -y percona-backup-mongodb

COPY ./start-agent.sh /start-agent.sh

USER nobody

ENTRYPOINT ["/start-agent.sh"]
CMD ["pbm-agent"]