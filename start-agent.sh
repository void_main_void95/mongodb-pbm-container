#!/bin/bash
set -o xtrace

if [ -z "${MONGODB_HOST}" ];then
  MONGODB_HOST="localhost:27017"
fi;

if [[ "${1:0:9}" = "pbm-agent" ]]; then
	OUT="$(mktemp)"
	OUT_CFG="$(mktemp)"
	PBM_MONGODB_URI="mongodb://${MONGODB_USER}:${MONGODB_PASS}@${MONGODB_HOST}/"
	timeout=5

	for i in {1..10}; do
		if [ "${SHARDED}" ]; then
			echo "waiting for sharded scluster"

			# check in case if shard has role 'shardsrv'
			set +o xtrace
			mongo "${PBM_MONGODB_URI}" --eval="db.isMaster().\$configServerState.opTime.ts" --quiet | tee "$OUT"
			set -o xtrace
			exit_status=$?

			# check in case if shard has role 'configsrv'
			set +o xtrace
			mongo "${PBM_MONGODB_URI}" --eval="db.isMaster().configsvr" --quiet | tail -n 1 | tee "$OUT_CFG"
			set -o xtrace
			exit_status_cfg=$?

			ts=$(grep -E '^Timestamp\([0-9]+, [0-9]+\)$' "$OUT")
			isCfg=$(grep -E '^2$' "$OUT_CFG")

			if [[ "${exit_status}" == 0 && "${ts}" ]] || [[ "${exit_status_cfg}" == 0 && "${isCfg}" ]]; then
				break
			else
				sleep "$((timeout * i))"
			fi
		else
			set +o xtrace
			mongo "${PBM_MONGODB_URI}" --eval="(db.isMaster().hosts).length" --quiet | tee "$OUT"
			set -o xtrace
			exit_status=$?
			rs_size=$(grep -E '^([0-9]+)$' "$OUT")
			if [[ "${exit_status}" == 0 ]] && [[ $rs_size -ge 1 ]]; then
				break
			else
				sleep "$((timeout * i))"
			fi
		fi
	done

  PBM_MONGODB_REPLSET=$(mongo "${PBM_MONGODB_URI}" --eval="rs.status().set" --quiet)
  PBM_MONGODB_URI="${PBM_MONGODB_URI}?replicaSet=${PBM_MONGODB_REPLSET}"

  if [[ $(mongo "${PBM_MONGODB_URI}" --quiet --eval="db.getSiblingDB('admin').getUser('${PBM_AGENT_MONGODB_USERNAME}')" == "null") ]]; then
      # create pbm role and pbm user
      mongo "${PBM_MONGODB_URI}" --quiet --eval="db.getSiblingDB('admin').createRole({ 'role': 'pbmAnyAction',
          'privileges': [
             { 'resource': { 'anyResource': true },
               'actions': [ 'anyAction' ]
             }
          ],
          'roles': []
       })"
      mongo "${PBM_MONGODB_URI}" --quiet --eval="db.getSiblingDB('admin').createUser({user: '${PBM_AGENT_MONGODB_USERNAME}',
             'pwd': '${PBM_AGENT_MONGODB_PASSWORD}',
             'roles' : [
                { 'db' : 'admin', 'role' : 'readWrite', 'collection': '' },
                { 'db' : 'admin', 'role' : 'backup' },
                { 'db' : 'admin', 'role' : 'clusterMonitor' },
                { 'db' : 'admin', 'role' : 'restore' },
                { 'db' : 'admin', 'role' : 'pbmAnyAction' }
             ]
          });"
  fi

  export PBM_MONGODB_URI="mongodb://${PBM_AGENT_MONGODB_USERNAME}:${PBM_AGENT_MONGODB_PASSWORD}@${MONGODB_HOST}/?authSource=admin&replicaSet=${PBM_MONGODB_REPLSET}"

  if [[ ${AWS_BUCKET} ]]; then
    if ! pbm config; then
      if [[ ${AWS_ACCESS_KEY_ID} ]] && [[ ${AWS_SECRET_ACCESS_KEY} ]]; then
        cat > pbm_config.yaml <<EOF
storage:
  type: s3
  s3:
    region: ${AWS_REGION}
    bucket: ${AWS_BUCKET}
    prefix: ${BACKUP_PREFIX}
    credentials:
      access-key-id: ${AWS_ACCESS_KEY_ID}
      secret-access-key: ${AWS_SECRET_ACCESS_KEY}
EOF
      else
         cat > pbm_config.yaml <<EOF
storage:
  type: s3
  s3:
    region: ${AWS_REGION}
    bucket: ${AWS_BUCKET}
    prefix: ${BACKUP_PREFIX}
EOF
      fi;
      pbm config --file pbm_config.yaml
      rm -rf pbm_config.yaml
    fi;
  fi;

	rm "$OUT"
else
    export PBM_MONGODB_URI="mongodb://${PBM_AGENT_MONGODB_USERNAME}:${PBM_AGENT_MONGODB_PASSWORD}@${MONGODB_HOST}/?authSource=admin"
fi

exec "$@"